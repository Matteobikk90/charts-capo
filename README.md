# Assignment

Build a full stack dashboard

A user can login or register in order to obtain a jwt, in order to be able to add, update or delete Charts with the help of Google Charts library.

The idea was to use Front end, back end and DB technologies by using modern programming languages like React.js, Node.js and PostgreSQL

## Approach

Firstly created the back end API with Node.js to check and return the jwt.
Creating GET, POST, PUT, DELETE and testing them through Postman, then moving to the Front End.

At the end I moved to the design, the use of proper semantic like <header>, <section>, ecc..... and to implement some animation through Particles.js and a keyframes

## Tech/Framework used

- React.js
- React Router
- Particles.js
- React Google Chart
- Axios
- CSS3
- ES6
- Node.js
- Knex
- PostgreSQL
- Jest
- Enzyme
- JWT

## How to run

- clone the repo and run npm start

## Issues

- Configure Jest
- Jwt
- Smaller component
- axios
