import React, { useState } from 'react';

const AddChart = (props) => {
  const { id, handleAddChart } = props;
  const [chart, setChart] = useState({
    data: '',
    title: '',
    user_id: id,
  });

  const handleChangeAddChart = (e) => {
    const value = e.target.value;
    setChart({
      ...chart,
      [e.target.name]: value,
    });
  };

  const handleBeautify = (e) => {
    e.preventDefault();
    let ugly = document.querySelector('textarea').value;
    let obj = JSON.parse(ugly);
    const pretty = JSON.stringify(obj, undefined, 4);
    document.querySelector('textarea').value = pretty;
  };

  const handleAddChartWrapper = (e) => {
    e.preventDefault();
    handleAddChart(chart);
  };

  return (
    <form autoComplete="none" className="card" onSubmit={handleAddChartWrapper}>
      <label htmlFor="chartTitle">Title</label>
      <input
        required
        id="chartTitle"
        type="text"
        name="title"
        value={chart.title || ''}
        onChange={handleChangeAddChart}
        //   validations={[required]}
      />
      <label htmlFor="chartData">Data</label>
      <p>Please use a json format</p>
      <textarea
        rows="6"
        required
        id="chartData"
        type="text"
        name="data"
        value={chart.data || ''}
        onChange={handleChangeAddChart}
        //   validations={[required]}
      />
      <div>
        <button onClick={handleBeautify}>Beautify</button>
        <input type="submit" value="Add" />
      </div>
    </form>
  );
};

export default AddChart;
