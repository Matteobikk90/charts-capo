import React, { Fragment, useState, useEffect } from 'react';

import AddChart from '../../Components/AddChart/AddChart';

import ChartsService from '../../services/chart.service';
import ChartComponent from '../Chart/Chart';

const Charts = (props) => {
  const { currentUser } = props;
  const [charts, setCharts] = useState([]);

  useEffect(() => {
    ChartsService.getCharts()
      .then((response) => setCharts(response))
      .catch((err) => console.log(err.message));
  }, []);

  const handleAddChart = (chart) => {
    ChartsService.addChart(currentUser.id, chart.data, chart.title)
      .then((response) => {
        setCharts([...charts, response]);
      })
      .catch((err) => console.log(err.message));
  };

  const handleDelete = (id) => {
    ChartsService.deleteChart(id)
      .then((chart) => {
        const clonedArray = [...charts];
        const updatedCharts = clonedArray.filter(
          (item) => item.id !== chart[0].id
        );
        setCharts((charts, updatedCharts));
      })
      .catch((err) => console.log(err.messsage));
  };

  const handleUpdate = (id, title, data) => {
    ChartsService.updateChart(id, title, data)
      .then((chart) => {
        const clonedArray = [...charts];
        const index = clonedArray.findIndex((el) => el.id === chart[0].id);
        clonedArray[index] = chart[0];
        setCharts(clonedArray);
      })
      .catch((err) => console.log(err.messsage));
  };

  return (
    <Fragment>
      <h1>Charts</h1>
      {charts.map((chart) => (
        <ChartComponent
          key={chart.id}
          data={chart}
          handleDelete={handleDelete}
          handleUpdate={handleUpdate}
        />
      ))}
      <AddChart handleAddChart={handleAddChart} />
    </Fragment>
  );
};

export default Charts;
