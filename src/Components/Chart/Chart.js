import React, { Fragment, useState } from 'react';

import { Chart } from 'react-google-charts';

import loader from '../../assets/logo/loader.svg';

const ChartComponent = (props) => {
  const { data, handleDelete, handleUpdate } = props;
  const [toggle, setToggle] = useState(false);
  const [chart, setChart] = useState({
    data: data.data,
    title: data.title,
  });

  const handleChangeUpdateChart = (e) => {
    let value = '';

    if (e.target.name === 'data') {
      value = JSON.parse(e.target.value);
    } else {
      value = e.target.value;
    }

    setChart({
      ...chart,
      [e.target.name]: value,
    });
  };

  const handleDeleteWrapper = (e) => {
    e.preventDefault();
    handleDelete(data.id);
  };

  const handleChangeUpdateChartWrapper = (e) => {
    e.preventDefault();
    setToggle(!toggle);
    handleUpdate(data.id, chart.title, chart.data);
  };

  const handleToggle = () => {
    setToggle(!toggle);
  };

  let options = {
    animation: {
      startup: true,
      easing: 'in',
      duration: 1000,
    },
    legend: {
      position: 'bottom',
    },
    title: data.title,
    chartArea: { width: '50%', height: '80%' },
  };

  if (data.data.type === 'is3D') {
    options = {
      ...options,
      is3D: true,
    };
  } else if (data.data.type === 'pieHole') {
    options = {
      ...options,
      pieHole: 0.4,
    };
  } else if (data.data.type === 'slices') {
    options = {
      ...options,
      // legend: 'none',
      pieSliceText: 'label',
      slices: {
        1: { offset: 0.2 },
        2: { offset: 0.3 },
        3: { offset: 0.4 },
        4: { offset: 0.5 },
      },
    };
  } else {
    options = {
      ...options,
    };
  }

  return (
    <article>
      {!toggle ? (
        <Fragment>
          <Chart
            width={'100%'}
            height={'300px'}
            chartType="PieChart"
            loader={<img src={loader} alt="Loader img" />}
            data={[
              [data.data.a, data.data.a],
              [data.data.a, data.data.b],
              [data.data.a, data.data.c],
              // [data.e, data.f],
              // [data.g, data.h],
            ]}
            options={options}
            rootProps={{ 'data-testid': data.id }}
          />
          <div>
            <button onClick={handleToggle}>Update</button>
            <button onClick={handleDeleteWrapper}>Delete</button>
          </div>
        </Fragment>
      ) : (
        <Fragment>
          <form
            autoComplete="none"
            className="card"
            onSubmit={handleChangeUpdateChartWrapper}
          >
            <label htmlFor="updateTitle">Title</label>
            <input
              required
              id="updateTitle"
              type="text"
              name="title"
              value={chart.title || ''}
              onChange={handleChangeUpdateChart}
              //   validations={[required]}
            />
            <label htmlFor="updateData">Data</label>
            <p>Please use a json format</p>
            <textarea
              required
              id="updateData"
              type="text"
              name="data"
              rows="6"
              //value={chart.data}
              value={JSON.stringify(chart.data, undefined, 4) || ''}
              onChange={handleChangeUpdateChart}
              //   validations={[required]}
            />
            <input type="submit" value="Update" />
          </form>
        </Fragment>
      )}
    </article>
  );
};

export default ChartComponent;
