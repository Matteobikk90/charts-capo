import React, { Fragment, useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from '../../pages/Home/Home';
import Login from '../../pages/Login/Login';
import Register from '../../pages/Register/Register';
import Dashboard from '../../pages/Dashboard/Dashboard';

import logo from '../../assets/logo/logo.svg';

import ProtectedRoute from '../ProtectedRoute/ProtectedRoute';

import AuthService from '../../services/auth.service';

const Navbar = () => {
  const [currentUser, setCurrentUser] = useState(null);
  const [isAutheticated, setisAutheticated] = useState(false);

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setisAutheticated(true);
      setCurrentUser(user);
    }
  }, []);

  const logOut = () => {
    AuthService.logout();
    setCurrentUser(null);
    setisAutheticated(false);
  };

  return (
    <Router>
      <header>
        <img src={logo} alt="Logo Full Stack Charts" />
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            {currentUser ? (
              <Fragment>
                <li onClick={logOut}>
                  <Link to="/login">Logout</Link>
                </li>
                <li>
                  <Link to="/dashboard">Dashboard</Link>
                </li>
              </Fragment>
            ) : (
              <Fragment>
                <li>
                  <Link to="/login">Login</Link>
                </li>
                <li>
                  <Link to="/register">Register</Link>
                </li>
              </Fragment>
            )}
          </ul>
        </nav>
      </header>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <ProtectedRoute
          exact
          path="/dashboard"
          component={Dashboard}
          auth={isAutheticated}
          currentUser={currentUser}
        />
      </Switch>
    </Router>
  );
};

export default Navbar;
