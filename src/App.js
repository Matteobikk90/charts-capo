import React, { Fragment } from 'react';
import Navbar from './Components/Navbar/Navbar';
import Particles from 'react-particles-js';

import './App.css';

export default function App() {
  return (
    <Fragment>
      <Navbar />
      <Particles
        className="particles"
        params={{
          particles: {
            number: {
              value: 60,
            },
            line_linked: {
              color: '#ffffff',
            },
            color: {
              value: '#3b3b3b',
            },
            size: {
              value: 1.5,
            },
          },
          interactivity: {
            events: {
              onhover: {
                enable: true,
                mode: 'repulse',
              },
            },
          },
        }}
      />
    </Fragment>
  );
}
