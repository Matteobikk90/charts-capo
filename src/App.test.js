import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElementSignin = getByText(/login/i);
  expect(linkElementSignin).toBeInTheDocument();
});

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElementRegister = getByText(/register/i);
  expect(linkElementRegister).toBeInTheDocument();
});
