import React, { useState, useRef } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';

import AuthService from '../../services/auth.service';

export default function Login(props) {
  const [loginDetails, setLoginDetails] = useState({
    username: '',
    password: '',
    loading: false,
    message: '',
  });

  const form = useRef();
  const checkBoxRef = useRef();

  const required = (value) => {
    if (!value) {
      return (
        <div className="alert alert-danger" role="alert">
          This field is required!
        </div>
      );
    }
  };

  const handleChangeSignIn = (e) => {
    const value = e.target.value;
    setLoginDetails({
      ...loginDetails,
      [e.target.name]: value,
    });
  };

  const handleSubmitLogin = (e) => {
    e.preventDefault();

    setLoginDetails({
      message: '',
      loading: true,
    });

    form.current.validateAll();

    if (checkBoxRef.current.context._errors.length === 0) {
      AuthService.login(loginDetails.username, loginDetails.password).then(
        () => {
          props.history.push('/dashboard');
          window.location.reload();
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setLoginDetails({
            loading: false,
            message: resMessage,
          });
        }
      );
    } else {
      setLoginDetails({
        loading: false,
      });
    }
  };

  return (
    <Form
      className="signIn"
      autoComplete="none"
      onSubmit={handleSubmitLogin}
      ref={form}
    >
      <h1>Login</h1>
      <label htmlFor="usernameInput">Username</label>
      <Input
        required
        id="usernameInput"
        type="text"
        name="username"
        value={loginDetails.username || ''}
        onChange={handleChangeSignIn}
        validations={[required]}
      />
      <label htmlFor="passwordInput">Password</label>
      <Input
        required
        id="passwordInput"
        type="password"
        name="password"
        value={loginDetails.password || ''}
        onChange={handleChangeSignIn}
        validations={[required]}
      />
      <input type="submit" value="Sign In" />

      {loginDetails.message && (
        <div className="alert alert-danger" role="alert">
          {loginDetails.message}
        </div>
      )}
      <CheckButton style={{ display: 'none' }} ref={checkBoxRef} />
    </Form>
  );
}
