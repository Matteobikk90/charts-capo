import React, { Fragment } from 'react';

import Charts from '../../Components/Charts/Charts';

const Dashboard = () => {
  const currentUser = JSON.parse(localStorage.getItem('user'));

  return (
    <Fragment>
      <section>
        <h1>Dashboard</h1>
        <h2>
          Welcome,
          <strong> {currentUser.username}</strong>
        </h2>
        <p>
          <strong>Token:</strong> {currentUser.accessToken.substring(0, 20)} ...{' '}
          {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
        </p>
        <p>
          <strong>Id:</strong> {currentUser.id}
        </p>
        <p>
          <strong>Email:</strong> {currentUser.email}
        </p>
      </section>
      <section className="chartsGrid">
        <Charts currentUser={currentUser} />
      </section>
    </Fragment>
  );
};

export default Dashboard;
