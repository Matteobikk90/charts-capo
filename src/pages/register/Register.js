import React, { useState, useRef } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';
import { isEmail } from 'validator';

import AuthService from '../../services/auth.service';

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const validEmail = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const vusername = (value) => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};

const vpassword = (value) => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        The password must be between 6 and 40 characters.
      </div>
    );
  }
};

export default function Register(props) {
  // const form = useRef();
  const checkBtn = useRef();

  const [registerDetails, setregisterDetails] = useState({
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
  });

  const [successRegister, setSuccessRegister] = useState({
    message: '',
    successful: false,
  });

  const handleChangeRegister = (e) => {
    const value = e.target.value;
    setregisterDetails({
      ...registerDetails,
      [e.target.name]: value,
    });
  };

  const handleSubmitRegister = (e) => {
    e.preventDefault();
    const { username, email, password, confirmPassword } = registerDetails;
    if (password === confirmPassword) {
      // form.current.validateAll();

      if (checkBtn.current.context._errors.length === 0) {
        AuthService.register(username, email, password).then(
          (response) => {
            setTimeout(() => {
              props.history.push('/login');
            }, 1000);
            setSuccessRegister({
              message: response.data.message,
              successful: true,
            });
          },
          (error) => {
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();

            setSuccessRegister({
              message: resMessage,
              successful: false,
            });
          }
        );
      }
    } else {
      return setSuccessRegister({
        message: 'Password do not match, check them before continue',
        successful: false,
      });
    }
  };

  return (
    <Form
      className="register"
      autoComplete="none"
      onSubmit={handleSubmitRegister}
    >
      <h1>Register</h1>
      <label htmlFor="nameRegister">Username</label>
      <Input
        required
        id="nameRegister"
        type="text"
        name="username"
        value={registerDetails.username || ''}
        onChange={handleChangeRegister}
        validations={[required, vusername]}
      />
      <label htmlFor="emailRegister">Email</label>
      <Input
        required
        id="emailRegister"
        type="email"
        name="email"
        value={registerDetails.email || ''}
        onChange={handleChangeRegister}
        validations={[required, validEmail]}
      />
      <label htmlFor="passwordRegister">Password</label>
      <Input
        required
        id="passwordRegister"
        type="password"
        name="password"
        value={registerDetails.password || ''}
        onChange={handleChangeRegister}
        validations={[required, vpassword]}
      />
      <label htmlFor="confirmPasswordRegister">Confirm Password</label>
      <Input
        required
        id="confirmPasswordRegister"
        type="password"
        name="confirmPassword"
        value={registerDetails.confirmPassword || ''}
        onChange={handleChangeRegister}
        validations={[required, vpassword]}
      />
      <input type="submit" value="Sign Up" />

      {successRegister.message && (
        <div
          className={
            successRegister.successful
              ? 'alert alert-success'
              : 'alert alert-danger'
          }
          role="alert"
        >
          {successRegister.message}
        </div>
      )}
      <CheckButton style={{ display: 'none' }} ref={checkBtn} />
    </Form>
  );
}
