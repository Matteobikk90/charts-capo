import React, { useState, useEffect } from 'react';
import UserService from '../../services/user.service';

export default function Home() {
  const [content, setContent] = useState('');

  useEffect(() => {
    UserService.getPublicContent().then(
      (response) => {
        setContent(response.data);
      },
      (error) => {
        const errContent =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setContent(errContent);
      }
    );
  }, []);

  return (
    <section>
      <h1>Home</h1>
      <p>{content}</p>
    </section>
  );
}
