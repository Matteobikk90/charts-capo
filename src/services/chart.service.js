import axios from 'axios';

const API_URL = 'http://localhost:3000/api/';

class ChartsService {
  getCharts() {
    const currentUser = JSON.parse(localStorage.getItem('user'));
    return axios
      .get(`${API_URL}getCharts/${currentUser.id}`)
      .then((response) => {
        return response.data;
      })
      .catch((err) => console.log(err.message));
  }

  addChart(user_id, data, title) {
    return axios
      .post(`${API_URL}addChart`, {
        user_id,
        data,
        title,
      })
      .then((response) => {
        return response.data;
      })
      .catch((err) => console.log(err.message));
  }

  deleteChart(id) {
    const data = JSON.stringify({ id: id });

    const config = {
      method: 'delete',
      url: 'http://localhost:3000/api/deleteChart',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    return axios(config)
      .then((res) => {
        return res.data;
      })
      .catch((err) => console.log(err.message));
    // return axios
    //   .delete(`${API_URL}deleteChart`, {
    //     data: { id: id },
    //   })
    //   .then((response) => {
    //     return response.data;
    //   })
    //   .catch((err) => console.log(err.message));
  }

  updateChart(id, title, data) {
    var dataa = JSON.stringify({
      title: title,
      data: data,
    });

    var config = {
      method: 'put',
      url: `http://localhost:3000/api/updateChart/${id}`,
      headers: {
        'Content-Type': 'application/json',
      },
      data: dataa,
    };

    return axios(config)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });

    // const config = { headers: { 'Content-Type': 'application/json' } };
    // return axios
    //   .put(`${API_URL}updateChart`, (data: value), config)
    //   .then((res) => {
    //     return res.data;
    //   })
    //   .catch((err) => console.log(err.message));
  }
}

export default new ChartsService();
